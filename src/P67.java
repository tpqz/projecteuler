import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Mateusz on 01.06.2017.
 */
public class P67 {
    public static void main(String[] args) throws Exception {
        int[][] triangle = create2DIntMatrixFromFile("p067_triangle.txt");
        System.out.println(Arrays.deepToString(triangle));
        findPath(triangle);
    }

    public static int[][] create2DIntMatrixFromFile(String filename) throws Exception {
        int[][] matrix = new int [104][100];
        InputStream stream = ClassLoader.getSystemResourceAsStream(filename);
        BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));


        String line;
        int row = 0;

        while ((line = buffer.readLine()) != null) {
            String[] vals = line.trim().split("\\s+");


            for(int i = 0; i < vals.length; i++){
                matrix[row][i] = Integer.parseInt(vals[i]);
               // System.out.print(matrix[row][i] + ", ");
            }
            //System.out.print("\n");
            row++;
        }

        int [] helper = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        for(int i = 0; i < 3; i++){
            row++;
            for(int j = 0; j < helper.length; j++) {
                matrix[row][i] = helper[i];
               // System.out.print(matrix[row][i] + ", ");
            }
           // System.out.print("\n");
        }

        return matrix;
    }

    private static void findPath(int[][] arr) {
        int lastIndex = 0;
        int value = 0;

        for (int i = 0; i < arr.length-3; i++) {
            lastIndex = getBestSector(arr, i, lastIndex);
            value += arr[i][lastIndex];
            System.out.println("Added: " + arr[i][lastIndex]);
            System.out.println(value);
            System.out.println("Row: " +  i);
            System.out.println("___________________");

        }

        System.out.println(value);
    }

    private static int getBestSector(int[][] arr, int startRow, int lastIndex) {
        int highestColIndex = 0;
        int max = 0;


        for (int a = 0; a < arr[startRow].length; a++) {
            if(a == lastIndex || a == lastIndex+1) {
                for (int x = 0; x < 8; x++) {
                    if (x == 0) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a]
                                + arr[startRow + 3][a];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 1) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a]
                                + arr[startRow + 3][a + 1];


                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 2) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 1];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 3) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 2];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 4) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 1];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 5) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 2];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 6) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 2]
                                + arr[startRow + 3][a + 2];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 7) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 2]
                                + arr[startRow + 3][a + 3];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    }
                }
            }
        }

        System.out.println("Max: " + max  + " column: " + (highestColIndex + 1));

        return highestColIndex;
    }
}
