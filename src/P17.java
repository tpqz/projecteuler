/**
 * Created by Mateusz on 31.05.2017.
 */
public class P17 {
    public static void main(String[] args) {
        System.out.println(countLetters(1000));
    }

    private static int countLetters(int num) {
        String[] numbers = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
        String[] teen = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        String[] decimals = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        String hundred = "hundred";
        String thousand = "thousand";

        int teens = 0;
        int dec = 0;
        int nums = 1;

        int result = 0;

        for (int i = 1; i <= num; i++) {
            System.out.print(i + " - ");
            if (i < 10) {
                System.out.println(numbers[i]);
                result += numbers[i].length();
            } else if (i > 9 && i < 20) {
                System.out.println(teen[teens]);
                result += teen[teens].length();
                teens++;
            } else if (i > 19 && i < 100) {
                teens = 0;
                if (Integer.toString(i).charAt(1) == '0') {
                    System.out.println(decimals[dec]);
                    result += decimals[dec].length();
                } else {
                    System.out.println(decimals[dec] + "-" + numbers[nums]);
                    result += decimals[dec].length() + numbers[nums].length();
                    nums++;
                }
                if (nums == 10) {
                    nums = 1;
                    dec++;
                }
            }else if(i > 99 && i < 1000){
                int hundreds = Integer.parseInt(Integer.toString(i).substring(0,1));
                int decs = Integer.parseInt(Integer.toString(i).substring(1,2));
                int units = Integer.parseInt(Integer.toString(i).substring(2,3));
                int test = Integer.parseInt(Integer.toString(decs) + Integer.toString(units));

                if(test == 0){
                    System.out.println(numbers[hundreds] + " " + hundred);
                    result += numbers[hundreds].length() + hundred.length();
                }

                if(test > 0 && test < 10){
                    System.out.println(numbers[hundreds] + " " + hundred + " and " + numbers[units]);
                    result += numbers[hundreds].length() + hundred.length() + "and".length() + numbers[units].length();
                }

                if(test > 9 && test < 20) {
                    System.out.println(numbers[hundreds] + " " + hundred + " and " + teen[units]);
                    result += numbers[hundreds].length() + hundred.length() + "and".length() + teen[units].length();
                }

                if(test >= 20){
                    System.out.println(numbers[hundreds] + " " + hundred + " and " + decimals[decs-2] + "-" + numbers[units]);
                    result += numbers[hundreds].length() + hundred.length() + "and".length() + decimals[decs-2].length() +  numbers[units].length();
                }
            }else{
                System.out.println("one " + thousand);
                result += "one".length() + thousand.length();
            }
        }
        return result;
    }
}
