import java.util.ArrayList;

/**
 * Created by Mateusz on 05.06.2017.
 */
public class P23 {
    public static void main(String[] args) throws InterruptedException {
        ArrayList<Integer> list;
        list = setAbundantArray();
        int sum = 0;

        for (int i = 0; i < 28300; i++) {
            if(!canBeWrittenAsSumOfAbundant(i, list)){
                sum += i;
            }
        }

        System.out.println("Sum:" + sum);
    }

    private static boolean canBeWrittenAsSumOfAbundant(int num, ArrayList<Integer> list) throws InterruptedException {

        for (int j = 0; j < list.size(); j++) {
            for (int a = 0; a < list.size(); a++) {
                if(list.get(j) + list.get(a) == num) {
                    return true;
                }else if(list.get(j) + list.get(a) > num){
                    break;
                }
            }
        }

        return false;
    }

    private static boolean isAbundant(int num) {
        int divisorsSum = 0;

        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                divisorsSum += i;
            }
        }

        return num < divisorsSum;
    }

    private static ArrayList<Integer> setAbundantArray() {
        ArrayList<Integer> arr = new ArrayList<>();

        for (int i = 1; i < 28124; i++) {
            if (isAbundant(i))
                arr.add(i);
        }

        return arr;
    }
}
