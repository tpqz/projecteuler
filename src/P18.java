/**
 * Created by Mateusz on 01.06.2017.
 */
public class P18 {
    public static void main(String[] args) {
        int[][] triangle = {{75},
                {95, 64},
                {17, 47, 82},
                {18, 35, 87, 10},
                {20, 04, 82, 47, 65},
                {19, 01, 23, 75, 03, 34},
                {88, 02, 77, 73, 07, 63, 67},
                {99, 65, 04, 28, 06, 16, 70, 92},
                {41, 41, 26, 56, 83, 40, 80, 70, 33},
                {41, 48, 72, 33, 47, 32, 37, 16, 94, 29},
                {53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14},
                {70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57},
                {91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48},
                {63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31},
                {04, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60, 04, 23},
                {0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0},
                {0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0},
                {0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0}};



        findPath(triangle);
    }

    private static void findPath(int[][] arr) {
        int lastIndex = 0;
        int value = 0;

        for (int i = 0; i < arr.length-3; i++) {
            lastIndex = getBestSector(arr, i, lastIndex);
            value += arr[i][lastIndex];
            System.out.println("Added: " + arr[i][lastIndex]);
        }

        System.out.println(value);
    }

    private static int getBestSector(int[][] arr, int startRow, int lastIndex) {
        int highestColIndex = 0;
        int max = 0;


        for (int a = 0; a < arr[startRow].length; a++) {
            if(a == lastIndex || a == lastIndex+1) {
                for (int x = 0; x < 8; x++) {
                    if (x == 0) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a]
                                + arr[startRow + 3][a];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 1) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a]
                                + arr[startRow + 3][a + 1];


                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 2) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 1];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 3) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 2];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 4) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 1];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 5) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 1]
                                + arr[startRow + 3][a + 2];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 6) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 2]
                                + arr[startRow + 3][a + 2];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    } else if (x == 7) {
                        int val = arr[startRow][a]
                                + arr[startRow + 1][a + 1]
                                + arr[startRow + 2][a + 2]
                                + arr[startRow + 3][a + 3];

                        if (val > max) {
                            max = val;
                            highestColIndex = a;
                        }
                    }
                }
            }
        }

        System.out.println("Max: " + max  + " column: " + (highestColIndex + 1));
        return highestColIndex;
    }
}
