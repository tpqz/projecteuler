import java.util.concurrent.Callable;

class PrimeCheck implements Callable<Long> {
    private Long num;

    PrimeCheck(long num){
        this.num = num;
    }


    private boolean isPrime(Long num){
        if(num == 1) return false;
        if(num == 2 ) return true;

        for(int i = 3; i <= num / 3; i+=2){
            if(num % i == 0)
                return false;
        }
        return true;
    }

    @Override
    public Long call() throws Exception {
        //System.out.println("Thread with num" + num + " started");
        if(isPrime(num)) {
            //System.out.println("Thread with num" + num + " ended");
            return num;
        }else return 0L;


    }
}