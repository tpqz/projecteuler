import java.util.ArrayList;

/**
 * Created by Mateusz on 02.06.2017.
 */
public class P21 {
    public static void main(String[] args) {
        int sum = 0;

        for (int i = 1; i < 100000; i++) {
            if (isAmicable(i)){
                sum += i;
                System.out.println("Added: " + i);
            }
        }

        System.out.println(sum);
    }

    private static int sumOfDividors(int num) {
        ArrayList<Integer> dividors = new ArrayList<>();
        int sumOfDividors = 0;

        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                dividors.add(i);
                sumOfDividors += i;
            }
        }

        return sumOfDividors;
    }

    private static boolean isAmicable(int num1) {
        int sumOfDivs = sumOfDividors(num1);

        if(sumOfDivs == num1) return false;

        int sumPair = sumOfDividors(sumOfDivs);

        return num1 == sumPair;
    }
}
