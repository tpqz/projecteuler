import java.math.BigInteger;

/**
 * Created by Mateusz on 30.05.2017.
 */
public class P16 {
    public static void main(String[] args) {
        BigInteger result = BigInteger.valueOf(1);
        String number = "";

        long num = 2;
        for(int i = 0; i < 100; i++) {
            BigInteger bigInteger = BigInteger.valueOf((long)Math.pow(num,10));
            result = result.multiply(bigInteger);
            System.out.println(result);
        }

        number = result.toString();
        int sum = 0;

        for(int i = 0; i < number.length(); i++){
            sum += Integer.parseInt(number.substring(i,i+1));
        }
        System.out.println(sum);
    }
}
