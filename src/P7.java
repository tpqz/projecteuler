import java.util.*;

public class p_7 {
    public static void main(String[] args) {
        int prime = 1000000;
        int counter=0;

        for (int i=2; i<prime; i++){
            boolean isPrime = true;

            for (int a=2; a<i; a++){
                if ( i%a == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime){
                counter = counter+1;
                System.out.println(counter + " - " + i);
                if(counter==10001){
                    System.exit(0);
                }
            }
        }

            }
        }