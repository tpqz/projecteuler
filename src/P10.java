import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.*;

/**
 * Created by Mateusz on 10.05.2017.
 */
public class P10 {
    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        long elapsedTime = 0L;

        long counter = 0;
        long result = 2;

        ExecutorService executorService = Executors.newFixedThreadPool(100);

        List<Callable<Long>> lst = new ArrayList<>();
        for (int i = 3; i <= 2000000; i += 2) {
            lst.add(new PrimeCheck(i));
        }
        // returns a list of Futures holding their status and results when all complete
        List<Future<Long>> tasks = executorService.invokeAll(lst);

        System.out.println(tasks.size() + " Responses recieved.\n");

        for(Future<Long> task : tasks)
        {
            result += task.get();
        }

        System.out.println(result);
        elapsedTime = (new Date()).getTime() - startTime;
        System.out.println("Time: " + elapsedTime);


        /* shutdown your thread pool, else your application will keep running */
        executorService.shutdown();
    }



//            for (int i = 1; i < 1000000; i+=2){
//                if (isPrime(i)) {
//                    System.out.println(i);
//                }
//            }
//    }

    private static boolean isPrime(long num){
        if(num == 1 ) return false;
        if(num == 2 ) return true;
        for(int i = 3; i <= num / 3; i+=2){
            if(num % i == 0)
                return false;
        }
        return true;
    }
}
