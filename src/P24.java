import com.sun.deploy.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Mateusz on 05.06.2017.
 */
public class P24 {
    public static void main(String[] argx){
        String [] test = {"0", "1", "2", "3"};
        String [][] test2 = new String[test.length * (test.length-1)][test.length];
        Arrays.sort(test);

//        for(int i = test.length-1; i >= 0; i--){
//            for(int j = test.length-1; j >= 1; j--){
        int counter = 0;
        for(int i = 0; i < test.length; i++){
            for(int j = 1; j < test.length; j++){
                System.out.println(counter + " " + Arrays.toString(test));
                test2[counter] = Arrays.copyOf(test, test.length);
                swap(test, j, j-1);

                //System.out.println(Arrays.toString(test) + counter);
                counter++;
            }
        }

        System.out.println(Arrays.deepToString(test2));

        for(int i = test.length-1; i >= 0; i--){
            final int a = i;
            Arrays.sort(test2,
                    Comparator.comparing((String[] entry) -> Double.parseDouble(entry[a]))
                            .reversed());
        }

        Collections.reverse(Arrays.asList(test2));
        System.out.println(Arrays.deepToString(test2));
    }



    public static final String[] swap (String[] a, int i, int j) {
        String t = a[i];
        a[i] = a[j];
        a[j] = t;

        return a;
    }
}
