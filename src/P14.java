/**
 * Created by Mateusz on 11.05.2017.
 */
public class P14 {
    public static void main(String[] args) {
        long biggest = 0;
        int index = 0;

        for (int i = 1; i < 100000; i++) {
            if (steps(i) > biggest) {
                biggest = steps(i);
                index = i;
            }
        }

        System.out.println("Index: " + index + " steps " + biggest);

    }

    private static long steps(long n) {
        int steps = 0;
        long temp = n;
            while (temp != 1) {
                steps++;
                if (temp % 2 == 0) temp = temp / 2;
                else temp = temp * 3 + 1;
            }


        return steps;
    }
}
