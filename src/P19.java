import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Mateusz on 05.06.2017.
 */
public class P19 {
    public static void main(String[] args) throws InterruptedException {
        Calendar c1 = GregorianCalendar.getInstance();
        Calendar finalDate = GregorianCalendar.getInstance();

        c1.set(1901,0,1);

        finalDate.set(2000,11,31);
        finalDate.set(Calendar.HOUR_OF_DAY, 0);
        finalDate.set(Calendar.MINUTE, 0);
        finalDate.set(Calendar.SECOND, 0);
        finalDate.set(Calendar.MILLISECOND, 0);

        int counter = 0;

        while (!c1.getTime().equals(finalDate.getTime())) {
            c1.add(Calendar.DAY_OF_MONTH, 1);
            c1.set(Calendar.HOUR_OF_DAY, 0);
            c1.set(Calendar.MINUTE, 0);
            c1.set(Calendar.SECOND, 0);
            c1.set(Calendar.MILLISECOND, 0);

            if(c1.get(Calendar.DAY_OF_MONTH) == 1 && c1.get(Calendar.DAY_OF_WEEK) == 1) {
                counter++;
                System.out.println(c1.getTime());
            }
        }

        System.out.println(counter);
    }
}
