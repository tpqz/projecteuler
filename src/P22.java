import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Mateusz on 03.06.2017.
 */

public class P22 {
    public static void main(String[] args) throws Exception {
        ArrayList<String> names = new ArrayList<>();
        names.addAll(createArrayFromFile("p022_names.txt"));
        Collections.sort(names.subList(0, names.size()));

        int score = getOverallScore(names);
        System.out.println(score);

    }

    public static ArrayList<String> createArrayFromFile(String filename) throws Exception {
        ArrayList<String> names = new ArrayList<>();

        InputStream stream = ClassLoader.getSystemResourceAsStream(filename);
        BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));


        String line;

        while ((line = buffer.readLine()) != null) {
            String[] vals = line.trim().split(",");

            Collections.addAll(names, vals);

        }
        return names;
    }

    private static int getNameValue(String name) {
        int nameValue = 0;
        char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        for (int i = 0; i < name.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (name.charAt(i) == Character.toUpperCase(alphabet[j])) {
                    nameValue += j + 1;
                }
            }
        }

        return nameValue;
    }

    private static int getOverallScore(ArrayList<String> arr) {
        int nameScore;
        int overallScore = 0;

        for(int i = 0; i < arr.size(); i++){
            nameScore = getNameValue(arr.get(i)) * (i + 1);
            System.out.println("Name: " + arr.get(i) + " Score: " + nameScore);
            overallScore += nameScore;
        }

        return overallScore;
    }
}
