/**
 * Created by Mateusz on 18.03.2016.
 */
public class p_6 {
    public static void main(String[] agrs){

        double sumOfSquare=0;
        double sumOfNatural=0;
        double squareOfNatural=0;

        for (int i = 1; i <= 100; i++){
           double square=Math.pow(i,2);
            sumOfSquare=sumOfSquare+square;
        }

        for (int i=1; i<= 100; i++){
            double natural=i;
            sumOfNatural=sumOfNatural+natural;
            squareOfNatural=Math.pow(sumOfNatural,2);
        }

        double result = squareOfNatural-sumOfSquare;
        int res = (int) Math.round(result);
        System.out.println(res);
    }
}
