/**
 * Created by Mateusz on 11.05.2017.
 */
public class P12 {
    public static void main(String[] args) {
        for (int i = 0; i < 50000; i++) {
            int count = 1;
            int triangular = decToTriangle(i);
            if (triangular % 2 == 0) {
                for (int j = 2; j < triangular; j++) {
                    if (triangular % j == 0)
                        count++;
                    if (count > 500) {
                        System.out.println("________________");
                        System.out.println("________________");
                        System.out.println("________________");
                        System.out.println("_______"+ count + "________");
                        System.out.println("_______"+ triangular + "________");
                        System.out.println("________________");
                        System.out.println("________________");
                        System.out.println("________________");
                        break;
                    }
                }
                if (count > 500) break;
                //System.out.println("Iteration " + i  + " Number: " + triangular + " num of dividers " + count);
            }
        }
    }

    private static int decToTriangle(int n) {
        int temp = 0;

        for (int i = 0; i <= n; i++) {
            temp += i;
        }

        return temp;
    }
}
