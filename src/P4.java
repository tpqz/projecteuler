/**
 * Created by Mateusz on 17.03.2016.
 */

public class p_4 {
    public static void main(String[] args) {
        int lowest = 100;
        int largest = 999;
        int mul;

            for (int i = 100; i <= largest; i++) {
                for (int a = 100; a<=largest; a++) {
                    mul = i * a;

                    String palindrome = Integer.toString(mul);
                    String first = palindrome.substring(0, palindrome.length() / 2);
                    String last = palindrome.substring(palindrome.length() / 2, palindrome.length());

                    String rev = new StringBuilder(last).reverse().toString();
                    int palNum = Integer.parseInt(palindrome);

                    if (first.equals(rev)) {
                        System.out.println(palNum + " ");
                    }
                }
        }
    }
}

