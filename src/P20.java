import java.math.BigInteger;

/**
 * Created by Mateusz on 31.05.2017.
 */
public class P20 {
    public static void main(String[] args){
        System.out.println(factorial(100));
        String value = factorial(100).toString();
        int goal = 0;

        for(int i = 0; i < value.length(); i++){
            goal += Integer.parseInt(value.substring(i,i+1));
        }

        System.out.println(goal);
    }

    private static BigInteger factorial(int num){
        BigInteger result = BigInteger.valueOf(1);

        for(int i = 1; i < num; i++){
            result = result.multiply(BigInteger.valueOf(i));
        }

        return result;
    }
}
