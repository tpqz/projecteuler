import java.util.stream.IntStream;

/**
 * Created by Mateusz on 14.03.2016.
 */

public class p1 {

        public static void main(String[] args) {
            int[] sum = new int[1000];

            for (int i = 1; i < 1000; i++ ){
                if (i%3 == 0 || i%5 == 0){
                    sum[i]=i;
                }
            }
                int suma = IntStream.of(sum).sum();
            System.out.print(suma + " ");
        }
    }