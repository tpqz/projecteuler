/**
 * Created by Mateusz on 14.03.2016.
 */
public class p_3 {


    public static void main(String[] args) {
        int prime = 1000;
        int primeArray[] = new int[90000000];
        long factor=600851475143L;
        boolean isPrime = true;

        for (int i = 1; i < primeArray.length; i++) {
            isPrime = true;

            for (int a = 2; a < i; a++) {
                if (i % a == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                primeArray[i] = i;
                if( factor%primeArray[i]==0 )
                System.out.print(primeArray[i] + " ");
            }
        }
    }
}